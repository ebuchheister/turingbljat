package de.wussen.turing;

public class ProduktionsregelLeft {

	private String state;
	private char readValue;
	private boolean endValue;

	public ProduktionsregelLeft(String currState, char readValue) {
		this(currState, readValue, false);
	}
	
	public ProduktionsregelLeft(String currState, char readValue, boolean endValue) {
		this.state = currState;
		this.readValue = readValue;
		this.endValue = endValue;
	}

	public String getState() {
		return state;
	}
	
	public char getReadValue() {
		return readValue;
	}
	
	public boolean isEndValue() {
		return endValue;
	}
	
}
