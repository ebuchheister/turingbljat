package de.wussen.turing;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JTextArea;

import de.wussen.binary.TuringProgram;

public class Turingmaschine {

	private List<Produktionsregel> prodR;
	private String startState;
	private String endState;
	private List<String> endStates;
	private char emptySymbol;
	private boolean barrier;
	private boolean trans;
	private volatile boolean run;

	public Turingmaschine(List<Produktionsregel> prodR, String startState, String endState) {
		this.prodR = prodR;
		this.startState = startState;
		this.endState = endState;
		this.emptySymbol = '#';
		this.barrier = false;
	}

	public Turingmaschine(List<Produktionsregel> prodR, String startState, List<String> endStates, char emptySymbol,
			boolean barrier, boolean transTable) {
		this.prodR = prodR;
		this.startState = startState;
		this.endStates = endStates;
		this.emptySymbol = emptySymbol;
		this.barrier = barrier;
		this.trans = transTable;
		this.run = true;
	}

	private String currState;
	private int pos;

	public char[] runTuring(char[] band, int pos) {

		this.currState = startState;
		this.pos = pos;

		while (!currState.equals(endState)) {
			printBand(band);
			step(band);
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		printBand(band);

		return band;
	}

	public void step(char[] band) {
		for (Produktionsregel regel : prodR) {
			if (regel.getLeft().getState().equals(currState) && regel.getLeft().getReadValue() == band[pos]) {
				band[pos] = regel.getRight().getWriteValue();
				pos += regel.getRight().getMove().getMove();
				currState = regel.getRight().getNewState();
				return;
			}
		}
	}

	public void step(ArrayList<Character> band) {
		for (Produktionsregel regel : prodR) {
			if (barrier && regel.getLeft().getState().equals(currState)
					&& regel.getLeft().getReadValue() == band.get(pos)) {
				if ((band.size() - 1 == pos && regel.getLeft().isEndValue())
						|| (band.size() - 1 != pos && !regel.getLeft().isEndValue())) {
					band.set(pos, regel.getRight().getWriteValue());
					pos += regel.getRight().getMove().getMove();
					currState = regel.getRight().getNewState();
					return;
				}
			} else if (regel.getLeft().getState().equals(currState)
					&& regel.getLeft().getReadValue() == band.get(pos)) {
//				if (!barrier && band.get(pos) == emptySymbol && regel.getRight().getWriteValue() != emptySymbol) {
//					if (pos == 0) {
//						band.add(pos++, emptySymbol);
//					} else {
//						band.add(emptySymbol);
//					}
//				}
				band.set(pos, regel.getRight().getWriteValue());
				pos += regel.getRight().getMove().getMove();
				if (!barrier && pos < 0) {
					band.add(++pos, emptySymbol);
				} else if (!barrier && pos >= band.size()){
					band.add(pos, emptySymbol);					
				}
				currState = regel.getRight().getNewState();
				return;
			}
		}
	}

	private void printBand(char[] band) {
		for (int i = 0; i < band.length; i++) {
			System.out.print("[" + band[i] + "]");
		}
		System.out.print("\n");

		for (int i = 0; i < band.length; i++) {
			if (i == pos) {
				System.out.print(" ↑ " + currState);
			} else {
				System.out.print("   ");
			}
		}
		System.out.print("\n");
	}

	private void printBand(ArrayList<Character> band, JTextArea ta) {
		if (trans) {
			ta.append("|- ");
		}
		
		for (int i = 0; i < band.size(); i++) {
			if (trans) {
				if (i == pos) {
					ta.append("\"" + currState + "\"" + band.get(i));
				} else {
					ta.append(String.valueOf(band.get(i)));
				}
			} else {
				ta.append("[" + band.get(i) + "]");
			}
		}

		if (!trans) {
			ta.append("\n");
			for (int i = 0; i < band.size(); i++) {
				if (i == pos) {
					ta.append(" ↑ " + currState);
				} else {
					ta.append("   ");
				}
			}
		}
		ta.append("\n");
		ta.repaint();
		ta.revalidate();
	}

	public ArrayList<Character> runTuring(ArrayList<Character> input, JTextArea ta, JFrame frame,
			char emptySymbol, int speed) {
		this.emptySymbol = emptySymbol;
		this.currState = startState;
//		this.pos = barrier ? 0 : 1;
		this.pos = 0;

		while (!isEndState(currState) && run) {
			printBand(input, ta);
			step(input);
			try {
				Thread.sleep(speed);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			frame.repaint();
		}
		printBand(input, ta);
		frame.repaint();

		return input;
	}

	public boolean isEndState(String currState) {
		for (String endState : endStates) {
			if (currState.equals(endState)) {
				return true;
			}
		}
		return false;
	}
	
	public void stop() {
		this.run = false;
	}
}
