package de.wussen.turing;

import de.wussen.binary.Move;

public class Produktionsregel {

	private ProduktionsregelLeft left;
	private ProduktionsregelRight right;
	
	public Produktionsregel(String currState, char readValue, String newState, char writeValue, Move move) {
		this(currState, readValue, false, newState, writeValue, false, move);
	}
	
	public Produktionsregel(String currState, char readValue, boolean readEndValue, String newState, char writeValue, boolean writeEndValue, Move move) {
		this.left = new ProduktionsregelLeft(currState, readValue, readEndValue);
		this.right = new ProduktionsregelRight(newState, writeValue, move, writeEndValue);
	}

	public ProduktionsregelLeft getLeft() {
		return left;
	}

	public ProduktionsregelRight getRight() {
		return right;
	}
	
}
