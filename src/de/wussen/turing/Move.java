package de.wussen.turing;

public enum Move {
	L((byte) -1), //
	R((byte) 1), //
	N((byte) 0);

	private byte move;

	private Move(byte move) {
		this.move = move;
	}

	public byte getMove() {
		return move;
	}
}
