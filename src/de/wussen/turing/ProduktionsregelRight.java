package de.wussen.turing;

import de.wussen.binary.Move;

public class ProduktionsregelRight {

	private String newState;
	private char writeValue;
	private boolean endValue;
	private Move move;

	public ProduktionsregelRight(String newState, char writeValue, Move move) {
		this(newState, writeValue, move, false);
	}
	
	public ProduktionsregelRight(String newState, char writeValue, Move move, boolean endValue) {
		this.newState = newState;
		this.writeValue = writeValue;
		this.move = move;
		this.endValue = endValue;
	}

	public String getNewState() {
		return newState;
	}

	public char getWriteValue() {
		return writeValue;
	}

	public Move getMove() {
		return move;
	}

	public boolean isEndValue() {
		return endValue;
	}
}
