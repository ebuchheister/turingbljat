package de.wussen.binary;

public class TuringBinaryCount {

	private static final char EMPTY = '#';

	private enum State {
		Z0("Z0"), Z1("Z1"), Z2("Z2"), ZE("Ze");
	
		private String state;
		
		private State(String state) {
			this.state = state;
		}
		
		public String getState() {
			return state;
		}
	}

	private enum Move {
		L((byte) -1), R((byte) 1), N((byte) 0);

		private byte move;

		private Move(byte move) {
			this.move = move;
		}

		public byte getMove() {
			return move;
		}
	}

	private static State state = State.Z0;

	public static void main(String[] args) {

		char[] band = new char[10];
		band[0] = EMPTY;
		band[1] = EMPTY;
		band[2] = '1';
		band[3] = '1';
		band[4] = '1';
		band[5] = '1';
		band[6] = EMPTY;
		band[7] = EMPTY;
		band[8] = EMPTY;
		band[9] = EMPTY;

		byte pos = 2;

		while (state != State.ZE) {
			printBand(band, pos);
			pos += next(pos, band);
		}
		printBand(band, pos);

	}

	private static void printBand(char[] band, int pos) {
		for (int i = 0; i < band.length; i++) {
			System.out.print("[" + band[i] + "]");
		}
		System.out.print("\n");

		for (int i = 0; i < band.length; i++) {
			if (i == pos) {
				System.out.print(state.getState() + "↑ ");
			} else if (i == pos-1) {
				System.out.print("  ");
			} else {
				System.out.print("   ");
			}
		}
		System.out.print("\n");
	}

	private static int next(byte pos, char[] band) {

		switch (state) {
		case Z0:
			if (band[pos] == '0' || band[pos] == '1') {
				return Move.R.getMove();
			} else if (band[pos] == EMPTY) {
				state = State.Z1;
				return Move.L.getMove();
			}
			break;

		case Z1:
			if (band[pos] == '0') {
				band[pos] = '1';
				state = State.Z2;
				return Move.L.getMove();
			} else if (band[pos] == '1') {
				band[pos] = '0';
				state = State.Z1;
				return Move.L.getMove();
			} else if (band[pos] == EMPTY) {
				band[pos] = '1';
				state = State.ZE;
				return Move.N.getMove();
			}
			break;

		case Z2:
			if (band[pos] == '0' || band[pos] == '1') {
				return Move.L.getMove();
			} else if (band[pos] == EMPTY) {
				state = State.ZE;
				return Move.R.getMove();
			}
			break;

		case ZE:
			return Move.N.getMove();

		default:
			System.err.println("Error!!!");
			break;
		}
		return Move.N.getMove();
	}

}
