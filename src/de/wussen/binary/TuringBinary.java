package de.wussen.binary;

import java.util.ArrayList;
import java.util.List;

import de.wussen.turing.Produktionsregel;
import de.wussen.turing.Turingmaschine;

public class TuringBinary {

	private static ArrayList<String> states;

	public static void main(String[] args) {
//		uebung8Aufgabe1();
		uebung8Aufgabe5();
	}
	
	private static void uebung8Aufgabe1() {
		List<Produktionsregel> prodR = new ArrayList<>();
		prodR.add(new Produktionsregel("Z0", '0', "Z0", '0', Move.R));
		prodR.add(new Produktionsregel("Z0", '1', "Z0", '1', Move.R));
		prodR.add(new Produktionsregel("Z0", '#', "Z1", '#', Move.L));
		prodR.add(new Produktionsregel("Z1", '1', "Z1", '0', Move.L));
		prodR.add(new Produktionsregel("Z1", '0', "Z2", '1', Move.L));
		prodR.add(new Produktionsregel("Z1", '#', "ZE", '1', Move.N));
		prodR.add(new Produktionsregel("Z2", '0', "Z2", '0', Move.L));
		prodR.add(new Produktionsregel("Z2", '1', "Z2", '1', Move.L));
		prodR.add(new Produktionsregel("Z2", '#', "ZE", '1', Move.R));

		Turingmaschine t = new Turingmaschine(prodR, "Z0", "ZE");
		t.runTuring(new char[] { '#', '1', '1', '1', '1', '0', '0', '0', '0', '#' }, 1);
	}

	private static void uebung8Aufgabe5() {
		List<Produktionsregel> prodR = new ArrayList<>();
		prodR.add(new Produktionsregel("ZS", '0', "Z0R", '0', Move.R));
		prodR.add(new Produktionsregel("ZS", '1', "Z1R", '1', Move.R));
		prodR.add(new Produktionsregel("Z0R", '0', "Z0R", '0', Move.R));
		prodR.add(new Produktionsregel("Z0R", '1', "Z0R", '1', Move.R));
		prodR.add(new Produktionsregel("Z0R", 'e', "Z0LW", 'e', Move.L));
		prodR.add(new Produktionsregel("Z0R", 'n', "Z0LW", 'n', Move.L));
		prodR.add(new Produktionsregel("Z0R", '#', "Z0LW", '#', Move.L));
		prodR.add(new Produktionsregel("Z1R", '0', "Z1R", '0', Move.R));
		prodR.add(new Produktionsregel("Z1R", '1', "Z1R", '1', Move.R));
		prodR.add(new Produktionsregel("Z1R", 'e', "Z1LW", 'e', Move.L));
		prodR.add(new Produktionsregel("Z1R", 'n', "Z1LW", 'n', Move.L));
		prodR.add(new Produktionsregel("Z1R", '#', "Z1LW", '#', Move.L));
		prodR.add(new Produktionsregel("Z0L", '0', "Z0L", '0', Move.L));
		prodR.add(new Produktionsregel("Z0L", '1', "Z0L", '1', Move.L));
		prodR.add(new Produktionsregel("Z0L", 'e', "Z0RW", 'e', Move.R));
		prodR.add(new Produktionsregel("Z0L", 'n', "Z0RW", 'n', Move.R));
		prodR.add(new Produktionsregel("Z0L", '#', "Z0RW", '#', Move.R));
		prodR.add(new Produktionsregel("Z1L", '0', "Z1L", '0', Move.L));
		prodR.add(new Produktionsregel("Z1L", '1', "Z1L", '1', Move.L));
		prodR.add(new Produktionsregel("Z1L", 'e', "Z1RW", 'e', Move.R));
		prodR.add(new Produktionsregel("Z1L", 'n', "Z1RW", 'n', Move.R));
		prodR.add(new Produktionsregel("Z1L", '#', "Z1RW", '#', Move.R));
		prodR.add(new Produktionsregel("Z0LW", '0', "Z0L", 'n', Move.L));
		prodR.add(new Produktionsregel("Z0LW", '1', "Z1L", 'n', Move.L));
		prodR.add(new Produktionsregel("Z0LW", 'e', "ZRF", 'e', Move.R));
		prodR.add(new Produktionsregel("Z0LW", 'n', "ZRF", 'n', Move.R));
		prodR.add(new Produktionsregel("Z1LW", '0', "Z0L", 'e', Move.L));
		prodR.add(new Produktionsregel("Z1LW", '1', "Z1L", 'e', Move.L));
		prodR.add(new Produktionsregel("Z1LW", 'e', "ZRF", 'e', Move.R));
		prodR.add(new Produktionsregel("Z1LW", 'n', "ZRF", 'n', Move.R));
		prodR.add(new Produktionsregel("Z0RW", '0', "ZR", 'n', Move.R));
		prodR.add(new Produktionsregel("Z0RW", '1', "ZR", 'n', Move.R));
		prodR.add(new Produktionsregel("Z0RW", 'e', "ZRF", 'e', Move.R));
		prodR.add(new Produktionsregel("Z0RW", 'n', "ZRF", 'n', Move.R));
		prodR.add(new Produktionsregel("Z1RW", '0', "ZR", 'e', Move.R));
		prodR.add(new Produktionsregel("Z1RW", '1', "ZR", 'e', Move.R));
		prodR.add(new Produktionsregel("Z1RW", 'e', "ZRF", 'e', Move.R));
		prodR.add(new Produktionsregel("Z1RW", 'n', "ZRF", 'n', Move.R));
		prodR.add(new Produktionsregel("ZR", '0', "Z0R", '0', Move.R));
		prodR.add(new Produktionsregel("ZR", '1', "Z1R", '1', Move.R));
		prodR.add(new Produktionsregel("ZR", 'e', "ZRF", 'e', Move.R));
		prodR.add(new Produktionsregel("ZR", 'n', "ZRF", 'n', Move.R));
		prodR.add(new Produktionsregel("ZRF", 'e', "ZRF", 'e', Move.R));
		prodR.add(new Produktionsregel("ZRF", 'n', "ZRF", 'n', Move.R));
		prodR.add(new Produktionsregel("ZRF", '#', "ZLF", '#', Move.L));
		prodR.add(new Produktionsregel("ZLF", 'e', "ZLF", '1', Move.L));
		prodR.add(new Produktionsregel("ZLF", 'n', "ZLF", '0', Move.L));
		prodR.add(new Produktionsregel("ZLF", '#', "ZE", '#', Move.R));
		prodR.add(new Produktionsregel("ZE", '0', "ZE", '0', Move.N));
		prodR.add(new Produktionsregel("ZE", '1', "ZE", '1', Move.N));
		prodR.add(new Produktionsregel("ZE", '#', "ZE", '#', Move.N));

		Turingmaschine t = new Turingmaschine(prodR, "ZS", "ZE");
		t.runTuring(new char[] { '#', '1', '1', '1', '1', '0', '0', '0', '0', '#' }, 1);
	}

}
