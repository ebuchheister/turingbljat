package de.wussen.binary;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import de.wussen.turing.Produktionsregel;
import de.wussen.turing.Turingmaschine;

public class TuringProgram extends JFrame implements Runnable {

	public static void main(String[] args) {
		new TuringProgram();
	}

	public class LeftPanel extends JPanel {

		private JTextArea ta;
		private JButton btnRun;

		public LeftPanel() {
			setPreferredSize(new Dimension(300, 425));
			setLayout(new BorderLayout());

			this.ta = new JTextArea();
			JScrollPane pane = new JScrollPane(null);
			pane.add(ta);

			ta.append("speed=50\n");
			ta.append("barrier=false\n");
			ta.append("barrierSymbol=^\n");
			ta.append("transFormation=false\n");
			ta.append("start=z0\n");
			ta.append("ende=ze\n");
			ta.append("leer=#\n");
			ta.append("input=000111\n");
			ta.append("\n");
			ta.append("Produktionsregeln:\n");

			btnRun = new JButton("Run");
			btnRun.setPreferredSize(new Dimension(300, 25));

			add(BorderLayout.CENTER, ta);
			add(BorderLayout.SOUTH, btnRun);
		}
	}

	public class RightPanel extends JPanel {

		private JTextArea ta;

		public RightPanel() {
			setLayout(new BorderLayout());
			this.ta = new JTextArea();
			ta.setEditable(false);
			JScrollPane pane = new JScrollPane(ta, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
					JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			Font font = new Font("Consolas", Font.BOLD, 20);
			ta.setFont(font);

			// char c = '1';
			// ta.append(String.valueOf(c) + "\n");
			// c |= 0b1000000000000000;
			// ta.append(String.valueOf(c) + "\n");
			// c &= 0b0111111111111111;
			// ta.append(String.valueOf(c) + "\n");

			pane.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
				public void adjustmentValueChanged(AdjustmentEvent e) {
					e.getAdjustable().setValue(e.getAdjustable().getMaximum());
				}
			});
			add(BorderLayout.CENTER, pane);
		}
	}

	private boolean barrier = false;
	private char barrierSymbol = '^';
	private String startState = "";
	private ArrayList<String> endStates;
	private char emptySymbol = '#';
	private boolean transTable = false;
	private int speed = 50;
	ArrayList<Character> input = new ArrayList<>();
	List<Produktionsregel> prodR = new ArrayList<>();

	LeftPanel leftPanel = new LeftPanel();
	RightPanel rightPanel = new RightPanel();

	public TuringProgram() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());

		leftPanel.btnRun.addActionListener(e -> {
			if (leftPanel.btnRun.getText().equals("Run")) {
				rightPanel.ta.setText("");
				barrier = false;
				startState = "";
				endStates = new ArrayList<>();
				emptySymbol = '#';
				input = new ArrayList<>();
				prodR = new ArrayList<>();

				for (String s : leftPanel.ta.getText().split("\\r?\\n")) {
					if (s.startsWith("barrier=")) {
						if (s.substring(8).equals("true")) {
							barrier = true;
						} else {
							barrier = false;
						}
					} else if (s.startsWith("speed=")) {
						speed = Integer.parseInt(s.substring(6));
					} else if (s.startsWith("transFormation=")) {
						if (s.substring(15).equals("true")) {
							transTable = true;
						} else {
							transTable = false;
						}
					} else if (s.startsWith("barrierSymbol=")) {
						barrierSymbol = s.substring(14).toCharArray()[0];
					} else if (s.startsWith("start=")) {
						startState = s.substring(6);
					} else if (s.startsWith("ende=")) {

						for (String p : s.substring(5).split("[ ]")) {
							endStates.add(p);
						}
					} else if (s.startsWith("leer=")) {
						emptySymbol = s.substring(5).toCharArray()[0];
					} else if (s.startsWith("input=")) {
//						if (!barrier) {
//							input.add(emptySymbol);
//						}
						for (int i = 6; i < s.length(); i++) {
							input.add(s.charAt(i));
						}
//						if (!barrier) {
//							input.add(emptySymbol);
//						}
					} else if (s.startsWith("δ") || s.startsWith("delta(")) {
						String currState = "";
						char readValue = 0;
						boolean readEndValue = false;
						String newState = "";
						char writeValue = 0;
						boolean writeEndValue = false;
						Move move = Move.N;
						int i = 0;
						for (String p : s.split("[δ,)=( ]")) {
							if (p.length() > 0) {
								if (i == 0) {
									currState = p;
								} else if (i == 1) {
									if (barrier) {
										if (p.toCharArray()[0] == '^') {
											readEndValue = true;
											readValue = p.toCharArray()[1];
										} else {
											readValue = p.toCharArray()[0];
										}
									} else {
										readValue = p.toCharArray()[0];
									}
								} else if (i == 2) {
									newState = p;
								} else if (i == 3) {
									if (barrier) {
										if (p.toCharArray()[0] == '^') {
											writeEndValue = true;
											writeValue = p.toCharArray()[1];
										} else {
											writeValue = p.toCharArray()[0];
										}
									} else {
										writeValue = p.toCharArray()[0];
									}
								} else if (i == 4) {
									if (p.equals("r") || p.equals("R")) {
										move = Move.R;
									} else if (p.equals("l") || p.equals("L")) {
										move = Move.L;
									} else {
										move = Move.N;
									}
								}
								++i;
							}
						}
						prodR.add(new Produktionsregel(currState, readValue, readEndValue, newState, writeValue,
								writeEndValue, move));
					}
				}
				t = new Thread(this);
				t.start();
				leftPanel.btnRun.setText("Stop");
			} else {
				t = null;
				tm.stop();
				tm = null;
				leftPanel.btnRun.setText("Run");
			}
		});

		add(BorderLayout.WEST, leftPanel);
		add(BorderLayout.CENTER, rightPanel);

		setSize(new Dimension(1024, 800));
		setVisible(true);
		setLocationRelativeTo(null);
	}

	private Thread t;
	private Turingmaschine tm;
	
	@Override
	public void run() {
		tm = new Turingmaschine(prodR, startState, endStates, emptySymbol, barrier, transTable);
		tm.runTuring(input, rightPanel.ta, this, emptySymbol, speed);
	}

}
