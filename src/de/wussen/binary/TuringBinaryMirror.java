package de.wussen.binary;

public class TuringBinaryMirror {

	private static final char EMPTY = '#';

	private enum State {
		ZS("Zs--"), //
		ZR("ZR--"), //
		ZRF("ZRF-"), //
		ZLF("ZLF-"), //
		Z0R("Z0R-"), //
		Z0RW("Z0RW"), //
		Z1R("Z1R-"), //
		Z1RW("Z1RW"), //
		Z0L("Z0L-"), //
		Z0LW("Z0W-"), //
		Z1L("Z1L-"), //
		Z1LW("Z1LW"), //
		ZE("ZE--");

		private String state;

		private State(String state) {
			this.state = state;
		}

		public String getState() {
			return state;
		}
	}

	private enum Move {
		L((byte) -1), R((byte) 1), N((byte) 0);

		private byte move;

		private Move(byte move) {
			this.move = move;
		}

		public byte getMove() {
			return move;
		}
	}
	
	

	private static State state = State.ZS;

	public static void main(String[] args) {

		// char[] band = new char[13];
		// band[0] = EMPTY;
		// band[1] = '0';
		// band[2] = '1';
		// band[3] = '1';
		// band[4] = '1';
		// band[5] = '0';
		// band[6] = '1';
		// band[7] = '0';
		// band[8] = '0';
		// band[9] = '0';
		// band[10] = '0';
		// band[11] = '1';
		// band[12] = EMPTY;

		char[] band = { EMPTY, '0', '1', '1', '0', '1', '0', '0', '1', '1', '1', '0', '1', '0', '1',
				EMPTY };
		byte pos = 1;

		while (state != State.ZE) {
			printBand(band, pos);
			pos += next(pos, band);
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		printBand(band, pos);

	}

	private static void printBand(char[] band, int pos) {
		for (int i = 0; i < band.length; i++) {
			System.out.print("[" + band[i] + "]");
		}
		System.out.print("\n");

		for (int i = 0; i < band.length; i++) {
			if (i == pos) {
				if (i == 0) {
					System.out.print(" ↑ " + state.getState());
				} else {
					System.out.print(state.getState() + "↑ ");
				}
			} else if (i == pos - 1) {
				System.out.print("");
			} else {
				System.out.print("   ");
			}
		}
		System.out.print("\n");
	}

	private static int next(byte pos, char[] band) {

		char c = band[pos];

		switch (state) {
		case ZS:
			if (c == '0') {
				state = State.Z0R;
				return Move.R.getMove();
			} else if (c == '1') {
				state = State.Z1R;
				return Move.R.getMove();
			}
			break;

		case Z0R:
			if (c == '1' || c == '0') {
				return Move.R.getMove();
			} else if (c == EMPTY || c == 'e' || c == 'n') {
				state = State.Z0LW;
				return Move.L.getMove();
			}
			break;

		case Z1R:
			if (c == '1' || c == '0') {
				return Move.R.getMove();
			} else if (c == EMPTY || c == 'e' || c == 'n') {
				state = State.Z1LW;
				return Move.L.getMove();
			}
			break;

		case Z0L:
			if (c == '1' || c == '0') {
				return Move.L.getMove();
			} else if (c == EMPTY || c == 'e' || c == 'n') {
				state = State.Z0RW;
				return Move.R.getMove();
			}
			break;

		case Z1L:
			if (c == '1' || c == '0') {
				return Move.L.getMove();
			} else if (c == EMPTY || c == 'e' || c == 'n') {
				state = State.Z1RW;
				return Move.R.getMove();
			}
			break;

		case Z0LW:
			if (c == '0') {
				state = State.Z0L;
				band[pos] = 'n';
				return Move.L.getMove();
			} else if (c == '1') {
				state = State.Z1L;
				band[pos] = 'n';
				return Move.L.getMove();
			} else if (c == 'n' || c == 'e') {
				state = State.ZRF;
				return Move.R.getMove();
			}
			break;

		case Z1LW:
			if (c == '0') {
				state = State.Z0L;
				band[pos] = 'e';
				return Move.L.getMove();
			} else if (c == '1') {
				state = State.Z1L;
				band[pos] = 'e';
				return Move.L.getMove();
			} else if (c == 'n' || c == 'e') {
				state = State.ZRF;
				return Move.R.getMove();
			}
			break;

		case Z0RW:
			if (c == '0' || c == '1') {
				state = State.ZR;
				band[pos] = 'n';
				return Move.R.getMove();
			} else if (c == 'n' || c == 'e') {
				state = State.ZRF;
				return Move.R.getMove();
			}
			break;

		case Z1RW:
			if (c == '0' || c == '1') {
				state = State.ZR;
				band[pos] = 'e';
				return Move.R.getMove();
			} else if (c == 'n' || c == 'e') {
				state = State.ZRF;
				return Move.R.getMove();
			}
			break;

		case ZR:
			if (c == '0') {
				state = State.Z0R;
				return Move.R.getMove();
			} else if (c == '1') {
				state = State.Z1R;
				return Move.R.getMove();
			} else if (c == 'e' || c == 'n') {
				state = State.ZRF;
				return Move.R.getMove();
			}
			break;

		case ZRF:
			if (c == 'e' || c == 'n') {
				return Move.R.getMove();
			} else if (c == EMPTY) {
				state = State.ZLF;
				return Move.L.getMove();
			}
			break;

		case ZLF:
			if (c == 'e') {
				band[pos] = '1';
				return Move.L.getMove();
			} else if (c == 'n') {
				band[pos] = '0';
				return Move.L.getMove();
			} else if (c == EMPTY) {
				state = State.ZE;
				return Move.R.getMove();
			}
			break;

		case ZE:
			return Move.N.getMove();

		default:
			System.err.println("Error!!!");
			break;
		}
		return Move.N.getMove();
	}

}
