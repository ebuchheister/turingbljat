Beispiel 1:

barrier=false
barrierSymbol=^
transFormation=false
start=z_0
ende=z_e
leer=⊠
input=1111

Produktionsregeln:
δ(z_0,0)=(z_0,0,R)
δ(z_0,1)=(z_0,1,R)
δ(z_0,⊠)=(z_1,⊠,L)
δ(z_1,1)=(z_1,0,L)
δ(z_1,0)=(z_2,1,L)
δ(z_1,⊠)=(z_e,1,N)
δ(z_2,0)=(z_2,0,L)
δ(z_2,1)=(z_2,1,L)
δ(z_2,⊠)=(z_e,⊠,R)


Beispiel 2:

barrier=true
barrierSymbol=^
transFormation=false
start=zs
ende=ze
leer=⊠
input=101101

Produktionsregeln:
δ(zs,0)  = (z0,a,R)
δ(zs,1)  = (z1,a,R)
δ(z0,0)   = (z0,0,R)
δ(z0,^0) = (z2,^0,N)
δ(z0,1)   = (z1,0,R)
δ(z0,^1) = (z2,^0,N)
δ(z1,0)   = (z0,1,R)
δ(z1,^0) = (z2,^1,N)
δ(z1,1)   = (z1,1,R)
δ(z1,^1) = (z2,^1,N)
δ(z2,0)   = (z2,0,L)
δ(z2,^0) = (z2,^0,L)
δ(z2,1)   = (z2,1,L)
δ(z2,^1) = (z2,^1,L)
δ(z2,a)   = (ze,0,N)
